FROM ubuntu:latest

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update && apt-get -y upgrade \
    && apt-get -y --no-install-recommends install apt-utils \
    && apt-get -y --no-install-recommends install git apt-transport-https ca-certificates \
    && update-ca-certificates

ADD check-dev-merge.sh /bin/onacy-check-dev-merge
RUN chmod +x /bin/onacy-check-dev-merge
