#!/usr/bin/env bash

set -e
set -o xtrace

# Support for merge request checks
[ -z "$CI_COMMIT_BRANCH" ] && CI_COMMIT_BRANCH=$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME

repoUrl=$(git config --get remote.origin.url)
git clone $repoUrl checkme
cd checkme
git checkout $CI_COMMIT_BRANCH
git fetch
FOUND=$(git log --merges $CI_DEFAULT_BRANCH..$CI_COMMIT_BRANCH | grep -P "(?i)merge.*'dev\/v?[0-9]+\.[0-9]+\.[0-9]+.*into" | wc -l | tr -d '\s\n\t\r ') && [[ "$FOUND" -eq '0' ]] || ( echo "!!! A dev branch was merged into this branch. This is not allowed !!!" && exit 1)
